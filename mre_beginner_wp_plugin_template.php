<?php
/**
 * Plugin Name: Plugin Template for Beginners
 * Plugin URI: https://bitbucket.org/eltresse/mre_beginner_wp_plugin_template
 * Description: A plugin ready template for beginners who want to create custom plugins.
 * Version: 1.0.0
 * Author: Mary Rose Elbambo
 * Author URI: https://www.facebook.com/maryeltressewebcustomizationservices
 */


class MRE_BeginnerTemplate {
   public function __construct() {

   }

   /*Create custom post type*/

   /*Create page template*/

   /*Register front end scripts and styles*/

   /*Register back end scripts and styles*/

}

$mre_beginner_template = new MRE_BeginnerTemplate();
