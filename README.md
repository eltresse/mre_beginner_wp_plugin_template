# README #

A plugin ready template for beginners who want to create custom plugins.

### What is this repository for? ###

* This is for WordPress beginners who wants to create custom plugin in WordPress.
* 1.0.0

### How do I get set up? ###

* Download this repository and put it inside WordPress wp-content/plugins directory.
* Rename the folder with the name of your plugin.
* Rename the file with same name of the plugin folder.
* Activate the plugin in WordPress Admin Plugins page.

### Contribution guidelines ###

* I will be updating this repository for more WordPress functionalities and make it simpler and easier to create a custom plugin.

### Who do I talk to? ###

* Mary Rose Elbambo
* https://www.facebook.com/maryeltressewebcustomizationservices
